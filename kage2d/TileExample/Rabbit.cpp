#include "Rabbit.h"
#include "example.h"
#include "Fireball.h"
#include "kage2dutil/anim.h"

Rabbit::Rabbit()
{
	m_sprite = kage::TextureManager::getSprite("data/gabe.png");
	//kage::centreOrigin(m_sprite);
	m_sprite->setOrigin(32, 36);
	m_sprite->setScale(0.5, 0.5);
	m_tags.add("Rabbit");
	m_physicsStyle = GameObject::e_psBox2D;

	m_anim.setSprite(m_sprite);
	m_anim.setGridSize(6, 2);
	m_anim.setFrameSize(64, 64);
	m_anim.addSequence("run", 0, 5, 0.5, true);
	m_anim.addSequence("idle", 6, 11, 0.5, true);
	m_anim.addSequence("jump", 3, 4, 0.5, false);


	// Make a Box2D body
	m_body = kage::Physics::BodyBuilder()
		.pos(0, 0)
		.userData(this)	// This lets the body know which GameObject owns it
		.build();

	// Make a fixture (collision shape) for the body
	kage::Physics::CircleBuilder()
		.radius(0.4)
		.mass(1)
		.userData(1)
		.build(m_body); // We need to tell the builder which body to attach to
}

Rabbit::~Rabbit()
{
	//Is the score apart of the  game or the player, the player can hold it's own score or it can be global
}

void Rabbit::update(float deltaT)
{
	// This code here it for throwing the fireball, which I have left incase I choose to use it later
	//if (sf::Mouse::isButtonPressed(sf::Mouse::Button::Left))
	//{
	//	for (int i = 0; i < 10; i++)
	//	{
	//		Fireball* fireball = kage::World::build<Fireball>();
	//		kf::Vector2 mousePos = sf::Mouse::getPosition(Example::inst().getWindow());
	//		mousePos /= kage::World::scale();

	//		kf::Vector2 dir = mousePos - position();
	//		dir.normalise();
	//		dir = dir.rotate(rand() / 40000);
	//		fireball->velocity(dir * 8);
	//		fireball->position(position());
	//	}
	//}

	kf::Vector2 v = velocity();
	v.x *= 0.95; // The Times .95 is to have a slow down style of movement
	// This is to get player movement to happen on set key press
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
	{
		v.x = -8;
		//addForce(-8, 0); 
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
	{
		v.x = 8;
		//addForce(8, 0);
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
	{
		v.y = -8; // Currently if held will making it some you go up constantly 
	}

	velocity(v);

	if (v.x == 0)
	{
		m_anim.play("idle", false);
	}
	else
	{
		m_anim.play("run", false);
		if (v.x < 0)
		{
			m_sprite->setScale(-0.5, 0.5);
		}
	}
	m_anim.update(deltaT);

}

void Rabbit::onCollision(GameObject *obj)
{
	if (obj->m_tags.has("Chest"))
	{
		//m_dead = true;		// kills itself
		obj->m_dead = true;	// kills the other object
		m_score += 10;
	}
	if (obj->m_tags.has("FrostGiant"))
	{
		//m_dead = true;		// kills itself
		//obj->m_dead = true;	// kills the other object
		float diffx = kf::sign(position().x - obj->position().x);
		float diffy = kf::sign(position().y - obj->position().y);

		if (diffy < -0.3)
		{
			obj->m_dead = true;
		}
		else
		{
			velocity(diffx * 5, 0);
			obj->velocity(-diffx * 5, 0);
			m_health--;
			if (m_health <= 0)
			{
				m_dead = true;
			}
		}
	}
}

void Rabbit::onCollision(b2Fixture *fix)
{
	//if ((int)(fix->GetUserData()) == 1234) // Fake ID value 1234
	//{
	//}
}

