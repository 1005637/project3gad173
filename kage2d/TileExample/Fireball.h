#pragma once
#include "kage2dutil/gameobject.h"

class Fireball : public kage::GameObject
{
public:
	Fireball();
	~Fireball();

	//void render();
	void update(float deltaT);
	void onCollision(GameObject *obj);
	void onCollision(b2Fixture *fix);
};
