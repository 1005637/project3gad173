#include "FrostGaint.h"
#include "example.h"
#include "Fireball.h" // only make by frost gaint.
#include "Rabbit.h"

FrostGaint::FrostGaint()
{
	m_sprite = kage::TextureManager::getSprite("data/frost.png");
	kage::centreOrigin(m_sprite);
	m_sprite->setScale(0.5, 0.5);
	m_tags.add("FrostGaint");

	m_physicsStyle = GameObject::e_psBox2D;

	// Make a Box2D body
	m_body = kage::Physics::BodyBuilder()
				.pos(0, 0)
				//.type(b2BodyType::b2_staticBody) Won't move at all, will sit in the air 
				.userData(this)	// This lets the body know which GameObject owns it
				.build();

	// Make a fixture (collision shape) for the body
	kage::Physics::CircleBuilder()
		.radius(0.4)
		.mass(1)
		.build(m_body); // We need to tell the builder which body to attach to
	kage::Physics::CircleBuilder()
		.pos(0,0.4)
		.sensor(true)
		.userData(1)
		.radius(0.1)
		.mass(1)
		.build(m_body); // We need to tell the builder which body to attach to
}

FrostGaint::~FrostGaint()
{

}

void FrostGaint::update(float deltaT)
{
	// Do logic here
	
	// This is applying the burn damage to the enemy
	if (m_burnTimer > 0)
	{
		//do Damage
		m_health -= 0.1; // health lost
		m_burnTimer -= deltaT; // counting down the timer
	}

	kf::Vector2 vel = velocity();

	bool OnGround = kage::Physics::getTouching(m_body, 1);
	b2Fixture* wallNextTo = kage::Physics::testPoint(position() + kf::Vector2(m_direction, 0));
	b2Fixture* wallNextToAbove = kage::Physics::testPoint(position() + kf::Vector2(m_direction, -1));
	b2Fixture* wallNextToBelow = kage::Physics::testPoint(position() + kf::Vector2(m_direction, 1));
	b2Fixture* wallNextToBelowFar = kage::Physics::testPoint(position() + kf::Vector2(m_direction * 2, 1));

	// This is to stop the enemy from falling of the edge.
	if (wallNextTo)
	{
		if (kage::Physics::getGameObject(wallNextTo))
		{
			if (wallNextToAbove)
			{
				m_direction = -m_direction;
			}
			else
			{
				if (OnGround)
				{
					vel.y = -4;
				}
			}
		}
	}
	if (OnGround)
	{
		if (!wallNextToBelow)
		{
			if (wallNextToBelowFar)
			{
				vel.y = -5;
			}
			else
			{
				m_direction = -m_direction;
			}
		}
	}
	// Chase the player
	Rabbit* player = (Rabbit*)kage::World::findByTag("Rabbit");
	if (player)
	{
		if (m_timer <= 0)
		{
			kage::Physics::Hit hit = kage::Physics::rayCast(position(), player -> position());
			if (hit.hit && kage::Physics::getGameObject(hit.fixture) == player)
			{

				Fireball* fireball = kage::World::build<Fireball>();
				kf::Vector2 dir = player->position() - position();
				dir.normalise();
				fireball->velocity(dir * 4);
				fireball->position(position() + dir);
				m_timer = 1;
			}
		}
	
	}
	vel.x = m_direction * 2.0f;
	velocity(vel);
}

void FrostGaint::onCollision(GameObject *obj)
{
	if (obj->m_tags.has("Player"))
	{
		//m_dead = true;		// kills itself
		//obj->m_dead = true;	// kills the other object
		m_burnTimer = 1.0; // Will start burnTimer on collison with player.
	}
}

void FrostGaint::onCollision(b2Fixture *fix)
{
	//if ((int)(fix->GetUserData()) == 1234) // Fake ID value 1234
	//{
	//}
}

