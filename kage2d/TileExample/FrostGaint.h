#pragma once
#include "kage2dutil/gameobject.h"

class FrostGaint : public kage::GameObject
{
public:
	FrostGaint();
	~FrostGaint();

	//void render();
	void update(float deltaT);
	void onCollision(GameObject *obj);
	void onCollision(b2Fixture *fix);

	float m_direction = -1.0f;
	float m_timer = 0;
	float m_burnTimer = 0;
	int m_health = 5;
};
