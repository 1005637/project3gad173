#include "TileMap.h"
#include "app.h"
#include "example.h"
#include <fstream>
using namespace std;

TileMap::TileMap()
{
	m_atlas.create("data/newatlas.png", 32, 32);
	SetTileSize(32, 32);

	TileDefinition tile;
	//Empty
	tile.blocking = false;
	tile.visibleInGame = true;
	tile.tileX = 0;
	tile.tileY = 0;
	m_tileDefinition.push_back(tile);
	// Lava
	tile.blocking = true;
	tile.visibleInGame = true;
	tile.tileX = 1;
	tile.tileY = 0;
	m_tileDefinition.push_back(tile);
	// Stone
	tile.blocking = true;
	tile.visibleInGame = true;
	tile.tileX = 2;
	tile.tileY = 0;
	m_tileDefinition.push_back(tile);
	// Red Stone
	tile.blocking = true;
	tile.visibleInGame = true;
	tile.tileX = 3;
	tile.tileY = 0;
	m_tileDefinition.push_back(tile);
	// Spawn Monster
	tile.blocking = false;
	tile.visibleInGame = true;
	tile.tileX = 4;
	tile.tileY = 0;
	m_tileDefinition.push_back(tile);
	// Spawn Player
	tile.blocking = false;
	tile.visibleInGame = true;
	tile.tileX = 5;
	tile.tileY = 0;
	m_tileDefinition.push_back(tile);
}


void TileMap::SetMapSize(int width, int height)
{
	m_tiles[0].resize(width * height);
	m_tiles[1].resize(width * height);
	m_mapHeight = height;
	m_mapWidth = width;
}

int TileMap::GetMapWidth()
{
	return m_mapWidth;
}

int TileMap::GetMapHeight()
{
	return m_mapHeight;
}

int TileMap::GetTileWidth()
{
	return m_tileWidth;
}

int TileMap::GetTileHeight()
{
	return m_tileHeight;
}

std::vector<TileDefinition>& TileMap::GetTileDefinitions()
{
	return m_tileDefinition;
}

void TileMap::Render(sf::RenderTarget& target)
{
	for (int layer = 0; layer < 2; ++layer)
	{
		for (int y = 0; y < m_mapHeight; y++)
		{
			for (int x = 0; x < m_mapWidth; x++)
			{
				int tile = GetTile(x, y, layer);
				TileDefinition def = m_tileDefinition[tile];
				m_atlas.draw(target, x * m_tileWidth, y * m_tileHeight, def.tileX, def.tileY);
			}
		}
	}
}

void TileMap::Clear(int tile, int layer)
{
	for(int y = 0; y < m_mapHeight; y++)
		for (int x = 0; x < m_mapWidth; x++)
		{
			SetTile(x, y, layer, tile);
		}	
}

int TileMap::GetTile(int x, int y, int layer)
{
	if (x >= 0 && x < m_mapWidth && y >= 0 && y < m_mapHeight && layer >= 0 && layer < 2)
	{
		return m_tiles[layer][x + y * m_mapWidth];
	}
	return 0;
}

void TileMap::SetTile(int x, int y, int layer, int tile)
{
	if (x >= 0 && x < m_mapWidth && y >= 0 && y < m_mapHeight && layer >= 0 && layer < 2)
	{
		m_tiles[layer][x + y * m_mapWidth] = tile;
	}
}

kage::Atlas& TileMap::GetAtlas()
{
	return m_atlas;
}

void TileMap::Load(string filename)
{
	fstream file;
	file.open(filename, std::ios::in);


	int width, height;
	file >> width;
	file.ignore(1);
	file >> height;
	file.ignore(1);

	for (int l = 0; l < 2; l++)
		for (int y = 0; y < m_mapHeight; ++y)
		{
			for(int x = 0; x < m_mapWidth; ++x)
			{
				int tile;
				file >> tile;
				file.ignore(1, ',');
				SetTile(x, y, 1, tile);

			}
		}
}

void TileMap::Save(string filename)
{
	fstream file;
	file.open(filename, std::ios::out);

	for (int l = 0; l < 2; l++)
		for (int y = 0; y < m_mapHeight; ++y)
		{
			for (int x = 0; x < m_mapWidth; ++x)
			{
				file << GetTile(x, y, 1);
				if (x != m_mapWidth - 1)
				{
					file << (",");
				}
			}
			file << std::endl;
		}
}

void TileMap::SetTileSize(int width, int Height)
{
	m_tileWidth = width;
	m_tileHeight = Height;
}
