#include "Fireball.h"
#include "example.h"

Fireball::Fireball()
{
	m_sprite = kage::TextureManager::getSprite("data/fireball.png");
	kage::centreOrigin(m_sprite);
	m_tags.add("Fireball");

	m_physicsStyle = GameObject::e_psBox2D;

	// Make a Box2D body
	m_body = kage::Physics::BodyBuilder()
				.pos(0, 0)
				.userData(this)	// This lets the body know which GameObject owns it
				.build();

	// Make a fixture (collision shape) for the body
	kage::Physics::CircleBuilder()
		.radius(0.2)
		.mass(1)
		.sensor(true)
		.build(m_body); // We need to tell the builder which body to attach to

	m_body->SetGravityScale(0);
	//This is the life span of the Gameobject, m_life
	m_life = 0.6;
}

Fireball::~Fireball()
{

}

void Fireball::update(float deltaT)
{
	// Do logic here
}

void Fireball::onCollision(GameObject *obj)
{
	if (obj->m_tags.has("Rabbit"))
	{
		m_dead = true;		// kills itself
		//obj->m_dead = true;	// kills the other object
	}
}

void Fireball::onCollision(b2Fixture *fix)
{
	m_dead = true;
	//if ((int)(fix->GetUserData()) == 1234) // Fake ID value 1234
	//{
	//}
}

