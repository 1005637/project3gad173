#include "example.h"

Example::Example(): App()
{
}

Example::~Example()
{
}

Example &Example::inst()
{
	static Example s_instance;
	return s_instance;
}

bool Example::start()
{
	// Floor, left wall and right wall static colliders.
	//kage::Physics::BoxBuilder().pos(kf::Vector2(15, 16)).size(30, 1).build(kage::Physics::getDefaultStatic());
	//kage::Physics::BoxBuilder().pos(kf::Vector2(0, 8)).size(1, 16).build(kage::Physics::getDefaultStatic());
	//kage::Physics::BoxBuilder().pos(kf::Vector2(30, 8)).size(1, 16).build(kage::Physics::getDefaultStatic());

	char* mapData[] = {
		"111111111111111111111111",
		"100000000000000000000001",
		"100000000000000000000001",
		"100000000000000000000001",
		"100000000000000000000001",
		"100000000000000000000001",
		"100011110000000000000001",
		"100010000000000000000001",
		"100010000000000000000001",
		"100010011110000000001111",
		"100010000000000000000001",
		"100010000000000000000001",
		"100011000000000002130001",
		"100010000000021111111141",
		"100000000211111111111111",
		"111111111111111111111111"
	};
	for (int y = 0; y < 16; ++y)
	{
		for (int x = 0; x < 24; ++x)
		{
			if (mapData[y][x] == '1')
			{
				kage::Physics::BoxBuilder().pos(kf::Vector2(x + 0.5, y + 0.5)).size(1, 1).userData(1).build(kage::Physics::getDefaultStatic());
			}
			if (mapData[y][x] == '2')
			{
				kage::Physics::createEdge(kage::Physics::getDefaultStatic(), kf::Vector2(x, y + 1), kf::Vector2(x + 1, y), 1, 0, (void *)2, 0, 0);
			}
			if (mapData[y][x] == '3')
			{
				kage::Physics::createEdge(kage::Physics::getDefaultStatic(), kf::Vector2(x, y), kf::Vector2(x + 1, y+1), 1, 0, (void *)3, 0, 0);
			}
			if (mapData[y][x] == '4')
			{
				kage::Physics::BoxBuilder().pos(kf::Vector2(x + 0.5, y + 0.5)).size(1, 1).userData(4).build(kage::Physics::getDefaultStatic());
			}
		}
	}


	m_backgroundSprite = kage::TextureManager::getSprite("data/sky.jpg");
	sf::Vector2u resolution = m_backgroundSprite->getTexture()->getSize();
	m_backgroundSprite->setScale(float(m_window.getSize().x) / resolution.x, float(m_window.getSize().y) / resolution.y);

	Rabbit *rabbit = kage::World::build<Rabbit>();
	rabbit->position(2, 4); // Note that this now uses metres instead of pixels.

	return true;
}

void Example::update(float deltaT)
{
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape) && m_window.hasFocus())
	{
		m_running = false;
	}

	ImGui::Begin("Kage2D");
	if(ImGui::Button("Clear"))
	{ 
		kage::World::clear();
	}
	ImGui::End();
}

void Example::render()
{
	//m_window.draw(*m_backgroundSprite);
	// The next line draws the physics debug info. This should be removed in a final release.
	kage::Physics::debugDraw(&m_window, 64);
}

void Example::cleanup()
{
	delete m_backgroundSprite;
}

