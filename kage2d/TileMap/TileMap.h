#pragma once
#include <vector>
#include "kage2dutil/physics.h"
#include "kage2dutil/atlas.h"

using namespace std;

struct TileDefinition
{
	bool blocking;
	bool visibleInGame;
	int tileX;
	int tileY;

};

class TileMap
{

public:

	int m_mapWidth;
	int m_mapHeight;
	int m_tileWidth;
	int m_tileHeight;
	vector<int> tiles[2];
	vector<TileDefinition> tileDefinition;
	kage::Atlas m_atlas;

	TileMap();
	virtual void SetMapSize(int width, int height);
	virtual void SetTileSize(int x, int layer, int title);
	int GetMapWidth();
	int GetMapHeight();
	int GetTileWidth();
	int GetTileHeight();

	vector<TileDefinition>& GetTileDefinitions();

	virtual void Render(sf::RenderTarget& target);
	void Clear(int tile, int layer);

	void Load(string filename);
	void Save(string filename);

	int GetTile(int x, int y, int layer);
	int SetTile(int x, int y, int layer, int tile);

	kage::Atlas& GetAtlas();

};